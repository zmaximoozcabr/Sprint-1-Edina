package test2;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.InputStream;

import org.junit.jupiter.api.Test;

import io.restassured.response.Response;
import io.restassured.module.jsv.JsonSchemaValidator;

public class JsonSchemaFeio {
	
	@Test
	public void deveValidarSchemaNoRetornoDeListaDeUsuarios() {
		Response response = when().get("https://compassuol.serverest.dev/usuarios");
		InputStream schema_to_validate = getClass().getClassLoader().getResourceAsStream("get_usuarios_200.json");
		assertThat(response.asString(), JsonSchemaValidator.matchesJsonSchema(schema_to_validate));
	}

}


































