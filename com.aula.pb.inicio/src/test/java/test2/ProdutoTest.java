package test2;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import model.Produto;

public class ProdutoTest {

    private static String token;

    @BeforeAll
    public static void setup() {
        // Autenticação
        token = given()
            .contentType(ContentType.JSON)
            .body("{ \"email\": \"seu_email@example.com\", \"password\": \"sua_senha\" }")
            .when()
            .post("https://serverest.dev/login")
            .then()
            .extract().path("authorization");
    }

    @Test
    public void deveCriarNovoProduto() {
        Produto produto = new Produto();
        produto.setNome("Produto de Teste");
        produto.setPreco(10.99);
        produto.setDescricao("Descrição do produto de teste");

        given()
            .contentType(ContentType.JSON)
            .header("Authorization", "Bearer " + token)
            .body(produto)
            .when()
            .post("https://serverest.dev/produtos")
            .then()
            .assertThat()
            .statusCode(201)
            .body("message", equalTo("Cadastro realizado com sucesso"));
    }

    @Test
    public void deveBuscarProdutoExistente() {
        // Supondo que o ID do produto seja conhecido
        String productId = "coloque_o_id_do_produto_aqui";

        given()
            .header("Authorization", "Bearer " + token)
            .when()
            .get("https://serverest.dev/produtos/" + productId)
            .then()
            .assertThat()
            .statusCode(200)
            .body("nome", equalTo("Produto de Teste"));
    }

    @Test
    public void deveAtualizarProdutoExistente() {
        // Supondo que o ID do produto seja conhecido
        String productId = "coloque_o_id_do_produto_aqui";

        Produto produto = new Produto();
        produto.setNome("Novo Nome do Produto");
        produto.setPreco(12.99);

        given()
            .contentType(ContentType.JSON)
            .header("Authorization", "Bearer " + token)
            .body(produto)
            .when()
            .put("https://serverest.dev/produtos/" + productId)
            .then()
            .assertThat()
            .statusCode(200);
    }

    @Test
    public void deveDeletarProdutoExistente() {
        // Supondo que o ID do produto seja conhecido
        String productId = "coloque_o_id_do_produto_aqui";

        given()
            .header("Authorization", "Bearer " + token)
            .when()
            .delete("https://serverest.dev/produtos/" + productId)
            .then()
            .assertThat()
            .statusCode(200);
    }
}
