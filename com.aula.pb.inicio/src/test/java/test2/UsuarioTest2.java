package test2;

import java.util.Locale;

import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import model.Usuario;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.github.javafaker.Faker;

import io.restassured.http.ContentType;
import lombok.Data;
public class UsuarioTest2 {
	private String user_id;
	
	@Test
	public void deveCriarNovoUsuarioFaker() {
	 Faker faker = new Faker(Locale.ENGLISH);
	 	Usuario usuario = new Usuario();
		usuario.setNome(faker.name().fullName());
		usuario.setEmail(faker.internet().emailAddress());
		usuario.setPassword(faker.internet().password());
		usuario.setAdministrador(true);
		
		given().
			body(usuario).
			contentType(ContentType.JSON).
		when().
			post("http://localhost:3000/usuarios").
		then().
			log().all().
			assertThat().
			statusCode(201).
		and().
			body("message", equalTo("Cadastro realizado com sucesso"));
	}

	@Test
	public void deveRetornarUsuarioExistente() {
		 Faker faker = new Faker(Locale.ENGLISH);
		 	Usuario usuario = new Usuario();
			usuario.setNome(faker.name().fullName());
			usuario.setEmail(faker.internet().emailAddress());
			usuario.setPassword(faker.internet().password());
			usuario.setAdministrador(true);
			
			String id = 
			given().
				body(usuario).
				contentType(ContentType.JSON).
			when().
				post("http://localhost:3000/usuarios").
			then().
				extract().jsonPath().getString("_id");
		
		when().
			get("http://localhost:3000/usuarios/{id}", id).
		then().
		assertThat().
		statusCode(200).
		and().
			body("nome", equalTo(usuario.getNome())).
		and().
		body("email", equalTo(usuario.getEmail()));
	}
	
	@Test
	public void devePegarUmUsuarioEspecifico() {
		user_id = "0uxuPY0cbmQhpEz1";
		Usuario created_user = new Usuario();
		created_user =
		when().
			get("https://compassuol.serverest.dev/usuarios/{id}", user_id).
			as(Usuario.class);
		
		assertThat(created_user.getNome(), equalTo("Fulano da Silva"));
		assertThat(created_user.getEmail(), equalTo("fulano@qa.com"));
		assertThat(created_user.getPassword(), equalTo("teste"));
	} 
	
	private static String token;
	
	 @BeforeAll
	    public static void setup() {
	        // Autenticação
	        token = given()
	            .contentType(ContentType.JSON)
	            .body("{ \"email\": \"seu_email@example.com\", \"password\": \"sua_senha\" }")
	            .when()
	            .post("https://serverest.dev/login")
	            .then()
	            .extract().path("authorization");
	    }
	
	}


