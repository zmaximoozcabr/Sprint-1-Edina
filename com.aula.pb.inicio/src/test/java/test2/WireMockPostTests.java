package test2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import model.Usuario;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

public class WireMockPostTests {
//     "nome": "{{{jsonPath request.body '$.nome'}}}",
//     "email": "{{{jsonPath request.body '$.email'}}}",
//     "password": "{{{jsonPath request.body '$.password'}}}",
//     "administrador": "{{{jsonPath request.body '$.administrador'}}}"
	
	@RegisterExtension
	static WireMockExtension wiremock = WireMockExtension.newInstance()
		.options(wireMockConfig().
				port(9999).
				extensions(new ResponseTemplateTransformer(true)))
		.build();
	
	@Test
	public void createsaNewUserTest() {
		Usuario user = new Usuario();
		user.setNome("mr potato head");
		user.setEmail("mrpotatohead@pixar.com");
		user.setPassword("qwerty123");
		user.setAdministrador(true);
		
		given().
			body(user).
		when().
			post("http://localhost:9999/usuarios").
		then().
			log().all().
		and().
			assertThat().
			statusCode(201).
		and().
			body("nome", equalTo(user.getNome())).
		and().
			body("email", equalTo(user.getEmail()));
	}
}
