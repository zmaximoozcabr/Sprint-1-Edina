package model;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

@Data
public class Produto {
    private String nome;
    private int preco;
    private String descricao;
    private int quantidade;
    @JsonProperty(value = "_id", access = Access.WRITE_ONLY)
    private String id;
}