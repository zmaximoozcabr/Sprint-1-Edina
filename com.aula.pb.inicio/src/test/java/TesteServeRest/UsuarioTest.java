package TesteServeRest;

import java.util.Locale;

import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import model.Usuario;

import org.junit.jupiter.api.Test;

import com.github.javafaker.Faker;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotationJsonProperty.Access;

import io.restassured.http.ContentType;
import lombok.Data;
import net.bytebuddy.NamingStrategy.Suffixing.BaseNameResolver.ForGivenType;
import net.bytebuddy.implementation.bytecode.assign.Assigner.EqualTypesOnly;

public class UsuarioTest {
	private String user_id;
	
	@Test
	public void deveCriarNovoUsuarioFaker() {
	 Faker faker = new Faker(Locale.ENGLISH);
	 	Usuario usuario = new Usuario();
		usuario.setNome(faker.name().fullName());
		usuario.setEmail(faker.internet().emailAddress());
		usuario.setPassword(faker.internet().password());
		usuario.setAdministrador(true);
		
		given().
			body(usuario).
			contentType(ContentType.JSON).
		when().
			post("http://localhost:3000/usuarios").
		then().
			log().all().
			assertThat().
			statusCode(201).
		and().
			body("message", equalTo("Cadastro realizado com sucesso"));
	}


	}